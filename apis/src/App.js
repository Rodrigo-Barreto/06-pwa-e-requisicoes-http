
import React from 'react';
import './App.css';
import ButtonItem from './components/button/buttonItem';
import InputItem from './components/input/inputItem';
import api from './utils/api';
import DataList from './components/dataList/dataList';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cepInput: '',
      cep: '',
      endereco: {},
      isValide: false,
      logradouro: '',
      bairro: '',
      localidade: '',
      uf: ''


    }
  }


  buscaCep = async () => {
    var validacep = /^[0-9]{8}$/;
    let bcCep = this.state.cepInput

    if (validacep.test(bcCep)) {
      try {

        this.setState({ isValide: true })
        let response = await api.get(bcCep + '/json/')
        let data = response.data

        this.setState(
          {
            endereco: {
              cep: data.cep,
              isValide: false,
              logradouro: data.logradouro,
              localidade: data.localidade,
              uf: data.uf,
            }
          }
        )

      } catch (error) {
        this.setState({ isValide: false })
        alert('buscaCep' + error)
      }

    }

  }



  render() {
    return <div className='body'>

      <div className='metade1'>
        <div>  <InputItem nome="CEP" changeValue={(value) => this.setState({ cepInput: value })} value={this.state.cepInput}></InputItem></div>
        <div>  <ButtonItem function={this.buscaCep}></ButtonItem></div>
      </div>
      <div className='metade2'>
      <div><DataList endereco={this.state.endereco} /></div>
      </div>

    </div>
  }
}

export default App;
